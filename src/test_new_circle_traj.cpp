#include "ros/ros.h"
#include "tf_utils/tf_utils.h"
#include "std_msgs/String.h"
#include "rc_status_msgs/RCChannels.h"
#include "ca_nav_msgs/PathXYZVPsi.h"

nav_msgs::Odometry lookahead_odo;
bool forced_replan = false;
bool rc_received = false;
bool cars_recvd = false;
int prev_rc_mode =  0;
bool odo_recvd = false;

double AngleDiff(double a,double b){
    double dif = fmod(b - a + M_PI,2*M_PI);
    if (dif < 0)
        dif += 2*M_PI;
    return dif - M_PI;
}

double ConstrainAngle(double x){
    x = fmod(x + M_PI,2*M_PI);
    if (x < 0)
        x += 2*M_PI;
    return x - M_PI;
}

ca_nav_msgs::PathXYZVPsi MakeSafeCircleTrajectory(Eigen::Vector3d current_pos, double current_heading,
                                                  double target_height, double radius, double heading_rate, double velocity,
                                                  int num_points, std::string frame){
    ca_nav_msgs::PathXYZVPsi path;
    path.header.stamp = ros::Time::now();
    path.header.frame_id = frame;
    ca_nav_msgs::XYZVPsi wp;
    wp.position.x = current_pos.x() + std::cos(current_heading)*radius*0.5;
    wp.position.y = current_pos.y() + std::sin(current_heading)*radius*0.5;
    wp.position.z = target_height;
    wp.vel = velocity;
    wp.heading = current_heading;
    //ROS_ERROR_STREAM("#0 WP::"<<wp.position.x<<"::"<<wp.position.y<<"::"<<wp.position.z);
    path.waypoints.push_back(wp);// First waypoint
    Eigen::Vector3d center = current_pos + Eigen::Vector3d((1.5*radius*cos(current_heading)), (1.5*radius*sin(current_heading)),0.);
    current_pos.x() = wp.position.x ; current_pos.y() = wp.position.y;
    current_pos.z() = wp.position.z;
    double angle_res = 2*M_PI/num_points;
    double angle_base = ConstrainAngle((current_heading + M_PI));
    for(size_t i=1; i<num_points; i++){
        // Start with the second waypoint
        double angle = angle_res*i + angle_base;
        Eigen::Vector3d next_pos = center + Eigen::Vector3d(radius*cos(angle), radius*sin(angle),0.);
        //ROS_ERROR_STREAM("Next Pos::"<<next_pos.x()<<"::"<<next_pos.y()<<"::"<<next_pos.z());
        Eigen::Vector3d dir = next_pos - current_pos; dir.normalize();
        double dir_heading = std::atan2(dir.y(),dir.x());        
        double angle_diff = AngleDiff(current_heading,dir_heading);
        double delt = std::fabs(angle_diff/heading_rate);
        //1. Align towards the path(dir_heading)
        current_pos = current_pos + velocity*dir*delt;
        wp.position.x = current_pos.x();
        wp.position.y = current_pos.y();
        wp.position.z = current_pos.z();
        wp.heading = dir_heading;
        path.waypoints.push_back(wp);
        //ROS_ERROR_STREAM(i<<"#1 WP::"<<wp.position.x<<"::"<<wp.position.y<<"::"<<wp.position.z);
        //2. Align towards the path
        Eigen::Vector3d next_dir = center - next_pos; next_dir.normalize();
        double next_heading = std::atan2(next_dir.y(),next_dir.x());        
        angle_diff = AngleDiff(dir_heading, next_heading);
        //ROS_ERROR_STREAM("Next heading::"<<next_heading<<" Path_heading::"<<dir_heading<<" Angle_diff::"<<angle_diff);
        delt = angle_diff/delt;
        current_pos = next_pos - velocity*dir*delt;
        wp.position.x = current_pos.x();
        wp.position.y = current_pos.y();
        wp.position.z = current_pos.z();
        wp.heading = dir_heading;
        path.waypoints.push_back(wp);
        //ROS_ERROR_STREAM(i<<"#2 WP::"<<wp.position.x<<"::"<<wp.position.y<<"::"<<wp.position.z);
        //3. Align towards the center
        current_pos = next_pos;
        wp.position.x = current_pos.x();
        wp.position.y = current_pos.y();
        wp.position.z = current_pos.z();
        wp.heading = next_heading;
        current_heading = next_heading;
        path.waypoints.push_back(wp);
        //ROS_ERROR_STREAM(i<<"#3 WP::"<<wp.position.x<<"::"<<wp.position.y<<"::"<<wp.position.z);
    }
    return path;
}



void RCCallback(const rc_status_msgs::RCChannels::ConstPtr& msg){
    int received_rc_mode = msg->mode;

    if(received_rc_mode!=-8000 && prev_rc_mode != received_rc_mode){
        rc_received = true;
    }
    else
        rc_received = false;
    prev_rc_mode = received_rc_mode;
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  lookahead_odo = *msg;
  odo_recvd = true;
}

void StringCallback(const std_msgs::String::ConstPtr& msg)
{
  forced_replan = true;
  rc_received = true;
}


int main(int argc, char **argv)
{
    forced_replan = false;

  ros::init(argc, argv, "test_circle_trajectory");
  ros::NodeHandle n("~");

  std::string forced_replan_topic;
  if (!n.getParam("forced_replan_topic", forced_replan_topic)){
    ROS_ERROR_STREAM("Could not get forced_replan_topic parameter.");
    return -1;
  }

  std::string radio_topic;
  if (!n.getParam("radio_topic", radio_topic)){
    ROS_ERROR_STREAM("Could not get radio_topic parameter.");
    return -1;
  }

  std::string path_topic;
  if (!n.getParam("path_topic", path_topic)){
    ROS_ERROR_STREAM("Could not get path_topic parameter.");
    return -1;
  }

  std::string pose_topic;
  if (!n.getParam("pose_topic", pose_topic)){
    ROS_ERROR_STREAM("Could not get pose_topic parameter.");
    return -1;
  }


  ros::Publisher path_pub = n.advertise<ca_nav_msgs::PathXYZVPsi>( path_topic, 0 );

  ros::Subscriber lookahead_sub = n.subscribe(pose_topic, 1, OdometryCallback);
  ros::Subscriber replan_sub = n.subscribe(forced_replan_topic,1,StringCallback);
  ros::Subscriber rc_sub = n.subscribe(radio_topic,1,RCCallback);


  std::string trajectory_frame = "/world";
  if (!n.getParam("trajectory_frame", trajectory_frame)){
    ROS_ERROR_STREAM("Could not get trajectory frame parameter.");
    return -1;
  }

  std::string param;
  Eigen::Vector3d current_pos;
  double target_height; double radius; double heading_rate; double velocity;
  int num_points;

  param = "target/target_radius";
  if (!n.getParam(param, radius)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  param = "target/target_height";
  if (!n.getParam(param, target_height)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  param = "target/target_speed";
  if (!n.getParam(param, velocity)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  param = "target/number_waypoints_around_target";
  if (!n.getParam(param, num_points)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  param = "heading_rate";
  if (!n.getParam(param, heading_rate)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  ros::Rate r(10);
  bool first = true;
  int count=0;
  while(ros::ok()){
      if((forced_replan || rc_received) && odo_recvd ){
          nav_msgs::Odometry odom = lookahead_odo;
          current_pos.x()= odom.pose.pose.position.x;
          current_pos.y()= odom.pose.pose.position.y;
          current_pos.z()= odom.pose.pose.position.z;
          tf::Pose pose;
          tf::poseMsgToTF(odom.pose.pose, pose);
          double current_heading = tf::getYaw(pose.getRotation());
          //current_pos.x() =0.; current_pos.y() =0.; current_pos.z() =0.;
          //current_heading = 0.;
          //target_height=  0.;
          ca_nav_msgs::PathXYZVPsi path = MakeSafeCircleTrajectory(current_pos, current_heading,
                                                                   current_pos.z(), radius, heading_rate, velocity,
                                                                   num_points, trajectory_frame);
          path_pub.publish(path);
          rc_received = false;
          forced_replan = false;
          first = false;
      }
      if(!odo_recvd) ROS_WARN_STREAM("safe_circle_traj::Waiting for odometry.");

      count++;
      ros::spinOnce();
      r.sleep();
  }
  return 0;
}
